angular
.module('app', [
    'lumx',
    'Factories',
    'Services',
    'Controllers'
    ]);


// Modules declaration
angular.module('Factories', ['ngResource']);
angular.module('Services', []);
angular.module('Controllers', []);



// Controllers
angular.module('Controllers').controller('MainController', ['$scope', 'DataService',
function($scope, DataService)
{
    // Toggle search mode
    $scope.searchMode = true;
    // Data result from search
    $scope.datas = [];
    // Data result to save
    $scope.data = {
        key: "",
        value: ""
    };
    $scope.message = "";

    /**
     * Call DataService to retrieve the value relative to the key in parameter.
     * @param  {String} key search key
     */
    $scope.search = function(key)
    {
        $scope.message = "";
        DataService.search(key, function(items)
        {
            $scope.datas = items
        });
    };

    /**
     * Save the data in the current scope.
     */
    $scope.save = function() {
        DataService.save($scope.data, function(resp) {
            $scope.searchMode = true;
            $scope.message = "Data successfully saved";
        });
    };
}]);


// Service
angular.module('Services').service('DataService', ['DataFactory',
function(DataFactory)
{
    var service = {};
    service.datas = {};

    /**
     * Search in memory. Make a server call if not found.
     * @param  {string}   key      search key
     * @param  {Function} callback success callback
     */
    service.search = function(key, callback) {
        let value = service.datas[key];
        if(value && callback) {
            callback([{key: key, value: value}]);
        }
        else {
            DataFactory.get({key: key}, function(resp)
            {
                for (let i = 0; i < resp.items.length; i++) {
                    let data = resp.items[i];
                    service.datas[data.key] = data.value;
                }

                if (callback) {
                    callback(resp.items);
                }
            });
        }
    };

    /**
     * Save the data in parameter by making a call to the server
     * @param  {dict} data data dictionary {key: "", value: ""}
     */
    service.save = function(data, callback) {
        DataFactory.save(data, function(resp) {
            service.datas[resp.key] = resp.value;

            if (callback) {
                callback(resp);
            }
        });
    };
    
    return service;
}]);

// HTTP Factory
angular.module('Factories').factory('DataFactory', function($resource)
{
    return $resource('/data', {},
    {
        get: { method: 'GET', isArray: false },
        save: { method: 'POST', isArray: false }
    });
});
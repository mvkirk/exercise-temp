import logging

from server.models import Data


def getDataByKey(keyString):
    """
    Return the data relative to the key in parameter.
    """
    data = Data.query(Data.keyString == keyString).get()
    return data


def getDataListByKey(keyString):
    """
    Return an array of data which begin by the key characters in parameters
    @return datas array
    """
    datas = Data.query(Data.keysString == keyString).fetch()
    return datas


def saveData(keyString, value):
    """
    Save a data relative to the keyString and value in parameter.
    @return data entity
    """
    data = getDataByKey(keyString)
    if not data:
        data = Data()

    data.keyString = keyString
    data.keysString = explodeKey(keyString)
    data.value = value
    data.put()
    logging.info("Data put: %s - %s", keyString, value)


    return data


def explodeKey(key):
    """
    Explode the in an array of character.
    Example : ['h', 'he', 'hel', 'hell', 'hello']
    @return exploded key array
    """
    entries = []
    entryTemp = ""
    for char in key:
        entryTemp += char
        entries.append(entryTemp)

    return entries
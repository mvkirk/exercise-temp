import logging
import os
import jinja2
import webapp2
import json
from google.appengine.ext.webapp.util import run_wsgi_app

import manager


jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(os.path.dirname(__file__))),
    variable_start_string='[[',
    variable_end_string=']]'
)


class MainHandler(webapp2.RequestHandler):
    """
    index.html handler
    """
    def get(self):
        template = jinja_environment.get_template('/client/front/index.html')
        self.response.out.write(template.render({}))


class DataHandler(webapp2.RequestHandler):
    """
    Data handler. Manage data request.
    GET to search
    POST to save
    """

    def get(self):
        """
        GET request to search on a key
        """
        key = self.request.get('key')
        logging.info("Searching for %s", key)
        datas = manager.getDataListByKey(key)
        response = {
            "items": [{'key': d.keyString, 'value': d.value} for d in datas]
        }

        self.response.write(json.dumps(response))


    def post(self):
        """
        Save a new data (key, value)
        """
        body = json.loads(self.request.body)
        key = body.get('key')
        value = body.get('value')
        response = {}
        if key and value:
            logging.info("Data to save: %s - %s", key, value)
            data = manager.saveData(key, value)
            response = {
                'key': data.keyString,
                'value': data.value
            }

        self.response.write(json.dumps(response))


application = webapp2.WSGIApplication([
    ('/data.*', DataHandler),
    ('/', MainHandler),
], debug=True)


def main():
    logging.getLogger().setLevel(logging.INFO)
    run_wsgi_app(application)

if __name__ == '__main__':
    main()

from google.appengine.ext import ndb


class Data(ndb.Model):
    """
    keyString because ndb.Model already has a real key property
    """
    keyString = ndb.StringProperty(required=True)
    keysString = ndb.StringProperty(repeated=True) # Exploded key array
    value = ndb.StringProperty()